// Теоретический вопрос
//
// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//
// Ответ: Document Object Model (DOM)  это представление документа HTML в виде дерева объектов, с помощью JavaScript можно изменять обьекты.

// Задание
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.

//     Технические требования:
//     Создать функцию, которая будет принимать на вход массив.
//     Каждый из элементов массива вывести на страницу в виде пункта списка
// Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//
//
//     Примеры массивов, которые можно выводить на экран:
//     ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//         ['1', '2', '3', 'sea', 'user', 23]

// Можно взять любой другой массив.

//     Не обязательное задание продвинутой сложности:
//
// //     ??? Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
//     Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.

// let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//
// let newArray = arr.map((item) => {
//     return document.write('<li>'+item+'</li>');
// });
//
// console.log(newArray);

let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
const newArray = `<ul class="arr">
    ${arr.map((item) => `<li>${item}</li>`).join('')}
</ul>`;

document.write(newArray);
console.log(newArray);


// window.onload = function() {
//     let my_timer = document.getElementById("my_timer");
//     let time = my_timer.innerHTML;
//     let arr = time.split(":");
//     let h = arr[0];
//     let m = arr[1];
//     let s = arr[2];
//     if (s === 0) {
//         if (m === 0) {
//             if (h === 0) {
//                 alert("Время вышло");
//                 window.location.reload();
//                 return;
//             }
//             h--;
//             m = 60;
//             if (h < 10) h = "0" + h;
//         }
//         m--;
//         if (m < 10) m = "0" + m;
//         s = 59;
//     }
//     else s--;
//     if (s < 10) s = "0" + s;
//     document.getElementById("my_timer").innerHTML = h+":"+m+":"+s;
//     setTimeout(startTimer, 1000);
// };

